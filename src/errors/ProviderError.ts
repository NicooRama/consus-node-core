import {AxiosError} from "axios";

export class ProviderError extends Error {
    axiosError: AxiosError;

    constructor(e: AxiosError, providerName: string) {
        super(`Error found on communicating with [${providerName}]`);
        this.name = this.constructor.name;
        this.axiosError = e;
        Error.captureStackTrace(e);
    }

    get wrappedMessage() {
        const responseData = this.axiosError?.response?.data;
        return responseData ? JSON.stringify(responseData) : 'Unknown error';
    }
}
