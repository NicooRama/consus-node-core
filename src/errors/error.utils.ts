import { ApiLogger } from "../logger";

export const uncaughtExceptionHandler = (logger: ApiLogger) => {
    process.on('uncaughtException', function (error) {
        logger.error('Uncaught Exception', {error})
    });}
