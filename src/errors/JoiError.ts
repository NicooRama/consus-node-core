export interface JoiErrors {
    details: JoiError[];
}

export interface JoiError {
    path: string[];
    message: string;
}
