export class Invalid {
    name: string;
    reason: string;

    constructor(name: string, reason: string) {
        this.name = name;
        this.reason = reason;
    }
}

export interface Invalids {
    params?: Invalid[];
    body?: Invalid[];
}