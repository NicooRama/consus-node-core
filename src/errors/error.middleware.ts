import { Request, Response, NextFunction } from 'express';
import {ApiLogger} from "../logger/logger.classes";
import {ApiError} from "./ApiError";
import {LoggedRequest} from "../logger/logger.interfaces";
import {ProviderError} from "./ProviderError";

/**
 * Crea un middleware para el manejo de errores segun la arquitectura definida para las aplicaciones de 123.
 * @param logger
 * @returns {(err: Error, req: e.Request, res: e.Response, next: e.NextFunction) => void}
 */
export const createErrorHandlerMiddleware = (logger: ApiLogger) => {
  return (err: Error, req: LoggedRequest, res: Response, next: NextFunction): Response => {
    if (err.name === 'ApiError') {
      const error = err as ApiError;
      const extraDetails = error.extraDetails();
      logger.error(`${error.message} ${extraDetails ? JSON.stringify(extraDetails) : '' }`, {req, error});
      return res.status(error.status).json({...error, name: undefined});
    }
    if (err.name === 'ProviderError') {
      const providerError = err as ProviderError;
      const responseData = providerError.axiosError?.response?.data;
      logger.error(responseData ? JSON.stringify(responseData) : 'Unknown error', {req, error: err})
    }

    next(err);
  };
};
