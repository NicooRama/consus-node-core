import {JoiError, JoiErrors} from "./JoiError";
import {ApiError} from "./ApiError";
import {Invalid} from "./Invalid";
import {constants} from "http2";

export const bodyError = (...invalidBodyAttributes: Invalid[]): ApiError => {
    return new ApiError(
            constants.HTTP_STATUS_BAD_REQUEST,
            'Form Error',
            'Errors have been found in the form data',
            { body: invalidBodyAttributes },
    );
};

export const paramsError = (...invalidParams: Invalid[]): ApiError => {
    return new ApiError(
            constants.HTTP_STATUS_BAD_REQUEST,
            'Params Error',
            'Errors have been found in url params',
            { params: invalidParams },
    );
};

const joiErrorsToInvalids = (joiErrors: JoiErrors): Invalid[] => {
    return joiErrors.details.map((e: JoiError) => ({ name: e.path.join('.'), reason: e.message }));
};

export const joiBodyError = (joiErrors: JoiErrors): ApiError => {
    return bodyError(...joiErrorsToInvalids(joiErrors));
};

export const joiParamsError = (joiErrors: JoiErrors): ApiError => {
    return paramsError(...joiErrorsToInvalids(joiErrors));
};