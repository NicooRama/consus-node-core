import {Invalid, Invalids} from "./Invalid";

export class ApiError extends Error {
    status: number;
    title: string;
    detail?: string;
    invalidBodyAttributes?: Invalid[];
    invalidParams?: Invalid[];

    constructor(status: number, title: string, detail: string, invalids?: Invalids) {
        super(`${title}${detail ? ` - ${detail}` : ''}`);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);

        this.status = status;
        this.title = title;
        this.detail = detail;
        if (invalids) {
            this.invalidParams = invalids.params;
            this.invalidBodyAttributes = invalids.body;
        }
    }

    extraDetails = () => {
        const extraDetails:any = {};
        if(this.invalidParams && this.invalidParams.length > 0){
            extraDetails.params = this.invalidParams
        }
        if(this.invalidBodyAttributes && this.invalidBodyAttributes.length > 0){
            extraDetails.body = this.invalidBodyAttributes
        }

        return extraDetails.params || extraDetails.body ? extraDetails : null;
    }
}
