import {existsInUserOrSystem} from "./validation";

export const ApiService = (
    model,
    mainProperty,
    validationSchema
) => {
    const saveManyOfUser = async (entities: any[], userId: string) => {
        entities = entities.filter(entity => !entity._id);
        let filteredEntities = [];
        for(let i=0; i<entities.length;i++) {
            const entity = entities[i];

            if (validationSchema.validate(entity).error) {
                continue;
            }

            const exists = await existsInUserOrSystem({
                model,
                entity,
                property: mainProperty,
                userId
            });

            if(exists){ continue; }

            filteredEntities.push({
                ...entity,
                userId,
            })
        };

        return await model.insertMany(filteredEntities, {lean: true});
    };

    return {
        saveManyOfUser
    }
}
