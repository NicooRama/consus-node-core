/**
 * Crea un objeto a partir de un string y un valor de asignacion
 * Ejemplo:
 * obj: {}
 * str: 'a.b.c'
 * val: value
 * Resultado:
 * {
 *  a: {
 *      b: {
 *          c: value
 *          }
 *     }
 * }
 * @param obj: objeto donde se va guardar la conversion
 * @param str: string que generara el objeto
 * @param val: valor de la propiedad
 * @returns {boolean}
 */
export const createObjectFromString = function(obj, str, val) {
    var keys, key;
    //make sure str is a string with length
    if (!str || !str.length || Object.prototype.toString.call(str) !== "[object String]") {
        return false;
    }
    if (obj !== Object(obj)) {
        //if it's not an object, make it one
        obj = {};
    }
    keys = str.split(".");
    while (keys.length > 1) {
        key = keys.shift();
        if (obj !== Object(obj)) {
            //if it's not an object, make it one
            obj = {};
        }
        if (!(key in obj)) {
            //if obj doesn't contain the key, add it and set it to an empty object
            obj[key] = {};
        }
        obj = obj[key];
    }
    obj[keys[0]] = val;
};