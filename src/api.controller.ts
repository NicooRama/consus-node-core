import {success} from "./responses";
import {NextFunction, Response} from "express";
import {MongoRequest} from "./middlewares/queryParams.middleware";
import {Types} from "mongoose";
import {LoggedRequest} from "./logger";
import {ApiLogging, emptyApiLogging} from "./api.logging";

export interface ApiMessages {
    created?: string,
    manyCreated?: string,
    updated?: string,
    removed?: string,
    notFound?: string,
}

export interface ApiControllerReturn {
    create: (Request, Response, NextFunction) => Promise<void>,
    createMany: (Request, Response, NextFunction) => Promise<void>,
    update: (Request, Response, NextFunction) => Promise<void>,
    find: (Request, Response, NextFunction) => Promise<void>,
    all: (Request, Response, NextFunction) => Promise<void>,
    allOfUser: (Request, Response, NextFunction) => Promise<void>,
    remove: (Request, Response, NextFunction) => Promise<void>
}

//TODO: deshardcodear userId key y name key
const filterDuplicatedBetweenSystemAndUserAggregation = () => {
    const aggregations = [];
    aggregations.push({
        $sort: {
            name: 1,
            userId: 1,
        }
    })

    /** Agrupo los que tiene el mismo nombre y obtengo el ultimo que, por el ordenamiento anterior, se que es el del usuario.
     * (el del usuario tiene mas prioridad que el del sistema)
     */
    aggregations.push({
            $group: {
                _id: {name: "$name"},
                entity: {$last: "$$ROOT"}
            }
        }
    )

    /**
     * Reemplazo el root con la estructura inicial, para no afectar las otras agregaciones.
     */
    aggregations.push(
        {$replaceRoot: {newRoot: "$entity"}}
    )
    return aggregations;
}

const addPopulationToStatement = (statement, populate) => {
    if (populate && populate.length > 0) {
        for (let i = 0; i < populate.length; i++) {
            if (Array.isArray(populate[i])) {
                statement.populate(...populate[i]);
            } else {
                statement.populate(populate[i]);
            }
        }
    }
}

export const ApiController = (
    model,
    messages: ApiMessages,
    logging: ApiLogging = emptyApiLogging(),
): ApiControllerReturn => {

    const create = async (req: LoggedRequest & MongoRequest, res: Response, next: NextFunction) => {
        logging.create.start(req);
        let entityCreated;
        try {
            entityCreated = await new model(req.body).save();
        } catch (error) {
            logging.create.error(req, error);
            return next(error);
        }
        entityCreated.__v = undefined;
        res.json(success(messages.created, entityCreated));
        logging.create.finish(req);
    };
    const createMany = async (req: LoggedRequest & MongoRequest, res: Response, next: NextFunction) => {
        logging.createMany.start(req);
        let entitiesCreated;
        try {
            entitiesCreated = await model.insertMany(req.body);
        } catch (error) {
            logging.createMany.error(req, error);
            return next(error);
        }
        entitiesCreated.__v = undefined;
        res.json(success(messages.manyCreated, entitiesCreated));
        logging.createMany.finish(req);
    };

    const all = async (req: MongoRequest & LoggedRequest, res: Response, next: NextFunction) => {
        logging.all.start(req);
        const {query, projection, sort, skip, limit, populate} = req.mongoParams;
        const statement = model.find(query).sort(sort);
        if (skip !== null && limit !== null) {
            statement.limit(limit).skip(skip);
        }
        addPopulationToStatement(statement, populate);
        statement.select(projection)
        let documents;
        try {
            documents = await statement.lean().exec();
        } catch (error) {
            logging.all.error(req, error);
            return next(error);
        }
        let count;
        try {
            count = await model.countDocuments(query);
        } catch (error) {
            logging.all.countError(req, error);
            return next(error);
        }
        res.setHeader('X-Total-Count', count);
        res.json(documents);
        logging.all.finish(req);
    };

    const allOfUser = async (req: MongoRequest & LoggedRequest, res: Response, next: NextFunction) => {
        logging.allOfUser.start(req);
        let {
            query,
            projection,
            sort,
            skip,
            limit,
            lookup
        } = req.mongoParams;

        const aggregations: any[] = [];
        aggregations.push(...filterDuplicatedBetweenSystemAndUserAggregation());
        if (sort) {
            aggregations.push({$sort: sort})
        }
        if (query) {
            aggregations.push({$match: query})
        }

        skip = !isNaN(skip) ? skip : 0;
        aggregations.push({$skip: skip})
        if (!isNaN(limit)) {
            aggregations.push({$limit: limit})
        }
        if (lookup) {
            aggregations.push(...lookup);
        }

        if (projection) {
            aggregations.push({$project: projection})
        }

        let documents;
        try {
            documents = await model.aggregate(aggregations).exec();
        } catch (error) {
            logging.allOfUser.error(req, error);
            return next(error);
        }
        //fixme: el count deberia ser otro aggregation
        let count
        try {
            count = await model.countDocuments(query);
        } catch (error) {
            logging.allOfUser.countError(req, error);
            return next(error);
        }
        res.setHeader('X-Total-Count', count);
        res.json(documents);
        logging.allOfUser.finish(req);
    };

    const find = async (req: MongoRequest & LoggedRequest, res: Response, next: NextFunction) => {
        logging.find.start(req);
        const {id} = req.params;
        const {projection, populate} = req.mongoParams;
        const statement = model.findById(id);
        addPopulationToStatement(statement, populate);
        statement.select(projection)
        let document
        try {
            document = await statement.lean().exec();
        } catch (error) {
            logging.find.error(req, error);
            return next(error);
        }
        res.json(document);
        logging.find.finish(req);
    };

    const update = async (req: LoggedRequest & MongoRequest, res: Response, next: NextFunction) => {
        logging.update.start(req);
        const {id} = req.params;
        let updated;

        try {
            const statatement = model.findOneAndUpdate(
                {
                    //@ts-ignore
                    _id: Types.ObjectId(id),
                },
                {$set: {...req.body}},
                {new: true}
            ).lean();
            addPopulationToStatement(statatement, req.mongoParams?.populate)
            updated = await statatement.exec();
        } catch (error) {
            logging.update.error(req, error);
            return next(error);
        }
        updated.__v = undefined;
        res.json(success(messages.updated, updated));
        logging.update.finish(req);
    };

    const remove = async (req: LoggedRequest & MongoRequest, res: Response, next: NextFunction) => {
        logging.remove.start(req);
        const {id} = req.params;
        let removed;
        try {
            removed = await model.deleteOne(
                {
                    //@ts-ignore
                    _id: Types.ObjectId(id),
                },
            );
        } catch (error) {
            logging.remove.error(req, error);
            return next(error);
        }
        const wasRemoved = removed.deletedCount > 0;
        let message;
        if (wasRemoved) {
            message = messages.removed;
            logging.remove.finish(req);
        } else {
            message = messages.notFound;
            logging.remove.finishWithoutChanges(req);
        }
        res.json(success(message, removed));
    };

    return {
        create,
        createMany,
        all,
        allOfUser,
        find,
        update,
        remove,
    }
};
