import {ApiController} from "../api.controller";
import {Types} from "mongoose";
import {ApiLogger} from "../logger";
import {v4 as uuidv4} from 'uuid'
import {apiLogging} from "../api.logging";
import {MongoMemoryServer} from "mongodb-memory-server";
import mongoose from "mongoose";


describe('ApiController', () => {
    const addReqLoggingParameters = (request) => {
        const req = {...request};
        if (!req.headers) {
            req.headers = {};
        }
        req.headers['x-forwarded-for'] = '127.0.0.1';
        if (!req.method) req.method = 'FOO';
        if (!req.originalUrl) req.originalUrl = 'bar/baz';
        req.logId = uuidv4();
        return req;
    }

    const messages = {
        created: 'foo created',
        updated: 'foo updated',
        removed: 'foo removed',
        notFound: 'foo not found',
    }

    const logging = apiLogging(new ApiLogger(), 'Document');

    const res = {
        json: jest.fn(),
        setHeader: jest.fn(),
    }

    const next = jest.fn();

    beforeEach(() => {
        res.json.mockClear();
        res.setHeader.mockClear();
    });

    describe('without mongo mock', () => {
        describe('create', () => {
            it('creates entity successfully', async () => {
                const save = jest.fn();
                const model: any = jest.fn().mockImplementation((entity) => ({
                    save: save.mockReturnValue(entity)
                }));
                const controller = ApiController(model, messages, logging);
                const req = addReqLoggingParameters({body: {name: 'foo'}});
                await controller.create(req, res, next);
                expect(model).toBeCalledWith(req.body);
                expect(save).toBeCalled();
                expect(res.json).toBeCalledWith({message: messages.created, data: req.body});
            });

            it('calls next when error occurs', async () => {
                const save = jest.fn();
                const model: any = jest.fn().mockImplementation(() => ({
                    save: save.mockRejectedValue(new Error('an error'))
                }));
                const controller = ApiController(model, messages, logging);
                const req = addReqLoggingParameters({body: {name: 'foo'}});
                await controller.create(req, res, next);
                expect(model).toBeCalledWith(req.body);
                expect(next).toBeCalledWith(new Error('an error'))
                expect(res.json).not.toBeCalled();
            });

            it('creates entity without logging', async () => {
                const save = jest.fn();
                const model: any = jest.fn().mockImplementation((entity) => ({
                    save: save.mockReturnValue(entity)
                }));
                const controller = ApiController(model, messages);
                const req = addReqLoggingParameters({body: {name: 'foo'}});
                await controller.create(req, res, next);
                expect(res.json).toBeCalled();
            })
        });

        describe('all', () => {
            const model: any = {};
            model.find = jest.fn().mockReturnValue(model);
            model.sort = jest.fn().mockReturnValue(model);
            model.limit = jest.fn().mockReturnValue(model);
            model.skip = jest.fn().mockReturnValue(model);
            model.select = jest.fn().mockReturnValue(model);
            model.populate = jest.fn().mockReturnValue(model);
            model.lean = jest.fn().mockReturnValue(model);
            model.exec = jest.fn();
            model.countDocuments = jest.fn();

            const controller = ApiController(model, messages, logging);

            beforeEach(() => {
                for (const key in model) {
                    model[key].mockClear();
                }
            });

            it('finds documents successfully', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents);
                model.countDocuments.mockResolvedValueOnce(2);
                await controller.all(req, res, next);
                expect(model.find).toBeCalledWith(req.mongoParams.query);
                expect(model.select).toBeCalledWith(req.mongoParams.projection);
                expect(model.sort).toBeCalledWith(req.mongoParams.sort);
                expect(model.countDocuments).toBeCalledWith(req.mongoParams.query);
                expect(res.setHeader).toBeCalledWith('X-Total-Count', 2)
                expect(res.json).toBeCalledWith(documents);
            });
            it('doesn\'t calls limit and skip when skip is null', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                        skip: null,
                        limit: 1000,
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents);
                model.countDocuments.mockResolvedValueOnce(2);
                await controller.all(req, res, next);
                expect(model.find).toBeCalledWith(req.mongoParams.query);
                expect(model.select).toBeCalledWith(req.mongoParams.projection);
                expect(model.sort).toBeCalledWith(req.mongoParams.sort);
                expect(model.limit).not.toBeCalled();
                expect(model.skip).not.toBeCalled();
                expect(model.countDocuments).toBeCalledWith(req.mongoParams.query);
                expect(res.setHeader).toBeCalledWith('X-Total-Count', 2)
                expect(res.json).toBeCalledWith(documents);
            });
            it('doesn\'t calls limit and skip when limit is null', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                        skip: 0,
                        limit: null,
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents);
                model.countDocuments.mockResolvedValueOnce(2);
                await controller.all(req, res, next);
                expect(model.find).toBeCalledWith(req.mongoParams.query);
                expect(model.select).toBeCalledWith(req.mongoParams.projection);
                expect(model.sort).toBeCalledWith(req.mongoParams.sort);
                expect(model.limit).not.toBeCalled();
                expect(model.skip).not.toBeCalled();
                expect(model.countDocuments).toBeCalledWith(req.mongoParams.query);
                expect(res.setHeader).toBeCalledWith('X-Total-Count', 2)
                expect(res.json).toBeCalledWith(documents);
            });
            it('calls limit and skip when limit and skip are not null', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                        skip: 0,
                        limit: 1000,
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents);
                model.countDocuments.mockResolvedValueOnce(2);
                await controller.all(req, res, next);
                expect(model.find).toBeCalledWith(req.mongoParams.query);
                expect(model.select).toBeCalledWith(req.mongoParams.projection);
                expect(model.sort).toBeCalledWith(req.mongoParams.sort);
                expect(model.limit).toBeCalledWith(1000);
                expect(model.skip).toBeCalledWith(0);
                expect(model.countDocuments).toBeCalledWith(req.mongoParams.query);
                expect(res.setHeader).toBeCalledWith('X-Total-Count', 2)
                expect(res.json).toBeCalledWith(documents);
            });
            it('calls next with error when exec find throws error', async () => {
                model.exec.mockRejectedValueOnce(new Error('exec error'));
                const req = addReqLoggingParameters({mongoParams: {}});
                await controller.all(req, res, next);
                expect(model.exec).toBeCalled();
                expect(next).toBeCalledWith(new Error('exec error'));
                expect(res.json).not.toBeCalled();
            });
            it('calls next with error when countDocuments throws error', async () => {
                model.exec.mockResolvedValue([]);
                model.countDocuments.mockRejectedValue(new Error('countDocuments error'));
                const req = addReqLoggingParameters({mongoParams: {}});
                await controller.all(req, res, next);
                expect(model.exec).toBeCalled();
                expect(next).toBeCalledWith(new Error('countDocuments error'));
                expect(res.json).not.toBeCalled();
            });
            it('finds documents without logging', async () => {
                const controller = ApiController(model, messages);
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents);
                model.countDocuments.mockResolvedValueOnce(2);
                await controller.all(req, res, next);
                expect(res.json).toBeCalled();
            })
        });

        describe('allOfUser', () => {
            const model: any = {};
            model.aggregate = jest.fn().mockReturnValue(model);
            model.exec = jest.fn().mockReturnValue(model);
            model.countDocuments = jest.fn();

            const controller = ApiController(model, messages, logging);

            beforeEach(() => {
                for (const key in model) {
                    model[key].mockClear();
                }
            });

            it('find all documents of user successfully', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents)
                model.countDocuments.mockResolvedValueOnce(2)
                await controller.allOfUser(req, res, next);
                expect(model.exec).toBeCalled();
                expect(model.countDocuments).toBeCalled();
                expect(res.setHeader).toBeCalledWith('X-Total-Count', 2);
                expect(res.json).toBeCalledWith(documents);
            })

            it('calls next with error when exec aggregate throws error', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                model.exec.mockRejectedValueOnce(new Error('exec error'))
                await controller.allOfUser(req, res, next);
                expect(model.exec).toBeCalled();
                expect(next).toBeCalledWith(new Error('exec error'))
                expect(res.json).not.toBeCalled();
            });

            it('calls next with error when countDocuments throws error', async () => {
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents)
                model.countDocuments.mockRejectedValueOnce(new Error('count error'))
                await controller.allOfUser(req, res, next);
                expect(model.exec).toBeCalled();
                expect(next).toBeCalledWith(new Error('count error'))
                expect(res.json).not.toBeCalled();
            });

            it('find all documents of user without logging', async () => {
                const controller = ApiController(model, messages);
                const req = addReqLoggingParameters({
                    mongoParams: {
                        query: {},
                        projection: '',
                        sort: '-name',
                    }
                });
                const documents = ['foo', 'bar'];
                model.exec.mockResolvedValueOnce(documents)
                model.countDocuments.mockResolvedValueOnce(2)
                await controller.allOfUser(req, res, next);
                expect(res.json).toBeCalled();
            })
        });

        describe('find', () => {
            const model: any = {};
            model.findById = jest.fn().mockReturnValue(model);
            model.select = jest.fn().mockReturnValue(model);
            model.populate = jest.fn().mockReturnValue(model);
            model.lean = jest.fn().mockReturnValue(model);
            model.exec = jest.fn();
            const req = (id) => (addReqLoggingParameters({
                params: {
                    id,
                },
                mongoParams: {
                    projection: ''
                }
            }));

            const controller = ApiController(model, messages, logging);

            it('finds document successfully', async () => {
                model.exec.mockResolvedValueOnce('foo');
                const id = new Types.ObjectId().toString();
                await controller.find(req(id), res, next);
                expect(model.findById).toBeCalledWith(id);
                expect(res.json).toBeCalledWith('foo');
            })

            it('calls next when findById throws error', async () => {
                model.exec.mockRejectedValue(new Error('find error'));
                const id = new Types.ObjectId().toString();
                await controller.find(req(id), res, next);
                expect(model.findById).toBeCalledWith(id);
                expect(next).toBeCalledWith(new Error('find error'));
                expect(res.json).not.toBeCalled();
            });

            it('finds document without logging', async () => {
                model.exec.mockResolvedValueOnce('foo');
                const controller = ApiController(model, messages);
                const id = new Types.ObjectId().toString();
                await controller.find(req(id), res, next);
                expect(model.findById).toBeCalledWith(id);
                expect(res.json).toBeCalled();
            })
        });

        describe('update', () => {
            const model: any = {};
            model.findOneAndUpdate = jest.fn().mockReturnValue(model);
            model.populate = jest.fn().mockReturnValue(model);
            model.lean = jest.fn().mockReturnValue(model);
            model.exec = jest.fn().mockReturnValue(model);

            const req = (id, body) => (addReqLoggingParameters({
                params: {
                    id,
                },
                body,
                mongoParams: {
                    projection: ''
                }
            }));

            const controller = ApiController(model, messages, logging);

            it('updates document successfully', async () => {
                model.exec.mockResolvedValueOnce({name: 'foo'});
                const id = new Types.ObjectId().toString();
                await controller.update(req(id, {name: 'foo'}), res, next);
                expect(model.findOneAndUpdate).toBeCalled();
                expect(res.json).toBeCalledWith({message: 'foo updated', data: {name: 'foo'}})
            })

            it('calls next when update throws error', async () => {
                model.exec.mockRejectedValue(new Error('update error'));
                const id = new Types.ObjectId().toString();
                await controller.update(req(id, {name: 'foo'}), res, next);
                expect(model.findOneAndUpdate).toBeCalled();
                expect(next).toBeCalledWith(new Error('update error'));
                expect(res.json).not.toBeCalled();
            })

            it('updates document without logging', async () => {
                model.exec.mockResolvedValueOnce({name: 'foo'});
                const controller = ApiController(model, messages);
                const id = new Types.ObjectId().toString();
                await controller.update(req(id, {name: 'foo'}), res, next);
                expect(model.findOneAndUpdate).toBeCalled();
                expect(res.json).toBeCalled();
            })
        })

        describe('remove', () => {
            const model = {
                deleteOne: jest.fn()
            };
            const req = (id) => (addReqLoggingParameters({
                params: {
                    id,
                },
            }));

            const controller = ApiController(model, messages, logging);

            it('removes a document successfully', async () => {
                model.deleteOne.mockResolvedValueOnce({deletedCount: 1});
                const id = new Types.ObjectId().toString();
                await controller.remove(req(id), res, next);
                //@ts-ignore
                expect(model.deleteOne).toBeCalledWith({_id: Types.ObjectId(id)});
                expect(res.json).toBeCalledWith({message: 'foo removed', data: {deletedCount: 1}});
            });

            it('doesn\'t remove the document because not exists', async () => {
                model.deleteOne.mockResolvedValueOnce({deletedCount: 0});
                const id = new Types.ObjectId().toString();
                await controller.remove(req(id), res, next);
                //@ts-ignore
                expect(model.deleteOne).toBeCalledWith({_id: Types.ObjectId(id)});
                expect(res.json).toBeCalledWith({message: 'foo not found', data: {deletedCount: 0}});
            });

            it('calls next when deleteOne throws error', async () => {
                model.deleteOne.mockRejectedValueOnce(new Error('delete error'));
                const id = new Types.ObjectId().toString();
                await controller.remove(req(id), res, next);
                //@ts-ignore
                expect(model.deleteOne).toBeCalledWith({_id: Types.ObjectId(id)});
                expect(next).toBeCalledWith(new Error('delete error'));
                expect(res.json).not.toBeCalled();
            });

            it('removes a document without logging', async () => {
                model.deleteOne.mockResolvedValueOnce({deletedCount: 1});
                const controller = ApiController(model, messages);
                const id = new Types.ObjectId().toString();
                await controller.remove(req(id), res, next);
                //@ts-ignore
                expect(model.deleteOne).toBeCalledWith({_id: Types.ObjectId(id)});
                expect(res.json).toBeCalledWith({message: 'foo removed', data: {deletedCount: 1}});
            });
        })
    });

    describe('with mongo mock', () => {
        let mongoServer: MongoMemoryServer;
        let model
        let modelToPopulate;
        let modelFromPopulate;

        beforeAll(async () => {
            mongoServer = await MongoMemoryServer.create();
            const mongoUri = mongoServer.getUri();

            await mongoose.connect(mongoUri, {});

            const modelSchema = new mongoose.Schema({
                name: String,
                userId: String,
            });

            model = mongoose.model('TestModel', modelSchema);

            const schemaFromPopulate = new mongoose.Schema({
                name: String,
            });

            modelFromPopulate = mongoose.model('TestModelFromPopulate', schemaFromPopulate);

            const schemaToPopulate = new mongoose.Schema({
                name: String,
                testModelId: {type: String, ref: 'TestModel'},
                testModelFromPopulateId: {type: String, ref: 'TestModelFromPopulate'},
            });

            modelToPopulate = mongoose.model('TestModelToPopulate', schemaToPopulate);
        });

        afterAll(async () => {
            await mongoose.disconnect();
            await mongoServer.stop();
        });

        beforeEach(async () => {
            await mongoose.model('TestModel').deleteMany({});
            await mongoose.model('TestModelToPopulate').deleteMany({});
            await mongoose.model('TestModelFromPopulate').deleteMany({});
        })

        describe('all', () => {
            describe('projection', () => {
                it('should returns only name field because projection is name string', async () => {
                    const req = addReqLoggingParameters({
                        mongoParams: {
                            query: {},
                            projection: 'name',
                            sort: '-name',
                        }
                    });
                    const _id = new Types.ObjectId();
                    await model({_id, name: 'foo', userId: '1'}).save();
                    const controller = ApiController(model, messages);
                    await controller.all(req, res, next);
                    expect(res.json).toBeCalledWith([{_id, name: 'foo'}])
                });
                it('should returns only name field because projection is {name: 1}', async () => {
                    const req = addReqLoggingParameters({
                        mongoParams: {
                            query: {},
                            projection: {name: 1},
                            sort: '-name',
                        }
                    });
                    const _id = new Types.ObjectId();
                    await model({_id, name: 'foo', userId: '1'}).save();
                    const controller = ApiController(model, messages);
                    await controller.all(req, res, next);
                    expect(res.json).toBeCalledWith([{_id, name: 'foo'}])
                });
                it('should returns full document because projection is empty', async () => {
                    const req = addReqLoggingParameters({
                        mongoParams: {
                            query: {},
                            projection: '',
                            sort: '-name',
                        }
                    });
                    const _id = new Types.ObjectId();
                    await model({_id, name: 'foo', userId: '1'}).save();
                    const controller = ApiController(model, messages);
                    await controller.all(req, res, next);
                    expect(res.json).toBeCalledWith([{_id, name: 'foo', userId: '1', __v: 0}])
                });
            });

            describe('populate', () => {
                it('should populate TestModelToPopulate with TestModel', async () => {
                    const testModelDocument = await model({name: 'testModel', userId: '1'}).save();
                    const testModelToPopulateDocument = await modelToPopulate({
                        name: 'testModelToPopulate',
                        testModelId: testModelDocument._id.toString()
                    }).save();
                    const req = addReqLoggingParameters({
                        mongoParams: {
                            query: {},
                            populate: ['testModelId'],
                        },
                    });
                    const controller = ApiController(modelToPopulate, messages);
                    await controller.all(req, res, next);
                    expect(res.json).toBeCalledWith([{
                        __v: 0,
                        _id: testModelToPopulateDocument._id,
                        name: 'testModelToPopulate',
                        testModelId: {_id: testModelDocument._id, name: 'testModel', userId: '1', __v: 0}
                    }]);
                });

                it('should populate TestModelToPopulate with TestModel and TestModelFromPopulate', async () => {
                    const testModelDocument = await model({name: 'testModel', userId: '1'}).save();
                    const testModelFromPopulateDocument = await modelFromPopulate({name: 'testModelFromPopulate'}).save();
                    const testModelToPopulateDocument = await modelToPopulate({
                        name: 'testModelToPopulate',
                        testModelId: testModelDocument._id.toString(),
                        testModelFromPopulateId: testModelFromPopulateDocument._id.toString()
                    }).save();
                    const req = addReqLoggingParameters({
                        mongoParams: {
                            query: {},
                            populate: ['testModelId', 'testModelFromPopulateId'],
                        },
                    });
                    const controller = ApiController(modelToPopulate, messages);
                    await controller.all(req, res, next);
                    expect(res.json).toBeCalledWith([{
                        __v: 0,
                        _id: testModelToPopulateDocument._id,
                        name: 'testModelToPopulate',
                        testModelId: {_id: testModelDocument._id, name: 'testModel', userId: '1', __v: 0},
                        testModelFromPopulateId: {
                            _id: testModelFromPopulateDocument._id,
                            name: 'testModelFromPopulate',
                            __v: 0
                        }
                    }]);
                })
            });
        });

        describe('update', () => {
            it('should update document', async () => {
                const _id = new Types.ObjectId();
                await model({_id, name: 'foo', userId: '1'}).save();
                const req = addReqLoggingParameters({
                    params: {
                        id: _id.toString(),
                    },
                    body: {
                        name: 'bar',
                    }
                });
                const controller = ApiController(model, messages);
                await controller.update(req, res, next);
                expect(res.json).toBeCalledWith({
                    message: 'foo updated',
                    data: {name: 'bar', userId: '1', _id, __v: undefined}
                })
            })

            it('should update document and return it populated', async () => {
                const testModelDocument = await model({name: 'testModel', userId: '1'}).save();
                const testModelToPopulateDocument = await modelToPopulate({
                    name: 'testModelToPopulate',
                    testModelId: testModelDocument._id.toString(),
                }).save();

                const req = addReqLoggingParameters({
                    mongoParams: {
                        populate: ['testModelId'],
                    },
                    params: {
                        id: testModelToPopulateDocument._id.toString(),
                    },
                    body: {
                        name: 'bar',
                    }
                });
                const controller = ApiController(modelToPopulate, messages);
                await controller.update(req, res, next);
                expect(res.json).toBeCalledWith({
                    message: 'foo updated', data:
                        {
                            name: 'bar',
                            _id: testModelToPopulateDocument._id,
                            testModelId: {_id: testModelDocument._id, name: 'testModel', userId: '1', __v: 0},
                            __v: undefined
                        }
                })
            })
        });
    });
})
