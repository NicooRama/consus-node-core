import {Request} from 'express';

export enum Level {
    INFO = 'info',
    ERROR = 'error',
}

export enum Result {
    SUCCESS = 'SUCCESS',
    FAIL = 'FAIL',
    IN_PROGRESS = 'IN_PROGRESS',
}

export interface Log {
    level: Level;
    message: string;
    error?: Error,
    req?: LoggedRequest;
    action?: string;
    result?: Result;
    params?: { [key: string]: any };
}

export interface LogInfoParams {
    req?: LoggedRequest,
    action?: string,
    result: Result,
    params?: { [key: string]: any };
}

export interface LogErrorParams {
    req?: LoggedRequest,
    action?: string,
    params?: { [key: string]: any };
    error: Error,
}

export interface LoggedRequest extends Request{
    logId: string;
}
