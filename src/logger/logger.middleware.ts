import {Request, Response, NextFunction} from 'express';
import { v4 as uuidv4 } from 'uuid'
import {LoggedRequest} from "./logger.interfaces";

export const logId = async (req: LoggedRequest, res: Response, next: NextFunction): Promise<void> => {
    req.logId = uuidv4();
    next();
}
