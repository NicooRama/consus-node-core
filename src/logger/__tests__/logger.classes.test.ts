import tk from "timekeeper";
import {ApiLogger} from "../logger.classes";
import {Result} from "../logger.interfaces";
import dateFormat from "date-format";

const dateMocked = new Date(1487076708000);
tk.freeze(dateMocked);

describe('logger.classes', () => {
    describe('ApiLogger', () => {
        const apiLogger: ApiLogger = new ApiLogger();
        const dateString = dateFormat.asString("dd-MM-yyyy hh:mm:ss.SSS", dateMocked);
        const consoleSpy = jest.spyOn((console as any)._stdout, 'write');

        const toWinstonTest= (expected: string) => `[${dateString}] - ${expected}\r\n`;

        beforeEach(() => {
            consoleSpy.mockReset();
        })

        it('log info with message, action and result successfully', () => {
            apiLogger.info("I'm a test", {
                action: 'TEST',
                result: Result.IN_PROGRESS,
            })

            expect(consoleSpy).toHaveBeenCalledWith(toWinstonTest("[INFO] - I'm a test - Action=TEST - Result=IN_PROGRESS - Params={}"));
        })

        it('log info with all params', () => {
            const req: any = {
                logId: "1",
                user: {
                    _id: '1s231g',
                },
                headers: {
                    ['x-forwarded-for']: "192.168.0.1"
                },
                method: "GET",
                originalUrl: "foo/bar"
            };
            apiLogger.info("I'm a test", {
                req,
                action: 'TEST',
                result: Result.IN_PROGRESS,
                params: {foo: 'bar'}
            });
            expect(consoleSpy).toHaveBeenCalledWith(toWinstonTest(`[INFO] - I'm a test - Id=${req.logId} - User=${req.user._id} - IP=${req.headers["x-forwarded-for"]} - Action=${req.method} ${req.originalUrl} - Result=IN_PROGRESS - Params={"foo":"bar"}`))
        })

    })
})
