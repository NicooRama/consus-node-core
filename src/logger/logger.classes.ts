import {format, Logger, loggers, transports} from 'winston';
import WinstonDailyRotateFile from 'winston-daily-rotate-file';
import {TransformableInfo} from 'logform';
import {Level, Log, LogErrorParams, LogInfoParams, Result} from "./logger.interfaces";
import {Request} from 'express';

const logFormat = format.combine(
    format.json(),
    format.timestamp({format: 'DD-MM-YYYY HH:mm:ss.SSS'}),
    format.printf(
        (log: TransformableInfo): string =>
            `[${log.timestamp}] - [${log.level.toUpperCase()}] - ${log.message}`,
    ),
);

/**
 * Crea un logger de winston
 * @param {string} filename
 * @returns {winston.Logger}
 */
export const createLogger = (filename: string): Logger => {
    const loggerTransports = [];
    if (filename) {
        loggerTransports.push(
            new WinstonDailyRotateFile({
                filename,
                datePattern: 'YYYY-MM-DD',
                level: 'info',
                maxSize: '5mb',
                maxFiles: '100',
            }),
        );
    }

    loggerTransports.push(
        new transports.Console({
            level: 'info',
        }),
    );

    loggers.add('ApiLogger', {
        format: logFormat,
        transports: loggerTransports,
    });

    return loggers.get('ApiLogger');
};

const getIp = (req: Request): string =>
    ((req.headers['x-forwarded-for'] || '') as string).split(',').pop().trim() ||
    (req as any).connection?.remoteAddress ||
    (req as any).socket?.remoteAddress;

export class ApiLogger {
    logger: Logger;

    constructor(filename?: string) {
        this.logger = createLogger(filename);
    }

    private log({
                    level,
                    req,
                    error,
                    action,
                    result,
                    message,
                    params = {},
                }: Log): void {
        const sections = [message];

        if(req) {
            sections.push(
                `Id=${req.logId ? req.logId : 'not-set'}`,
                `User=${(req as any).user ? (req as any).user._id : 'unauthenticated'}`,
                `IP=${getIp(req)}`,
            )
        }

        sections.push(
            `Action=${req ? `${req.method} ${req.originalUrl}` : action}`,
            `Result=${result}`,
            `Params=${JSON.stringify(params)}`,
        )

        if(error) {
            sections.push(`Error=${error.message}. ${error.stack}`);
        }

        this.logger.log(
            level,
            sections.join(' - '),
        );
    }

    info(message: string, {
        req,
        action,
        result,
        params = {},
    }: LogInfoParams): void {
        this.log({level: Level.INFO, message, action, result, req, params});
    }

    error(
        message: string,
        {
            req,
            error,
            action,
            params,
        }: LogErrorParams,
    ): void {
        this.log({level: Level.ERROR, req, message, action, result: Result.FAIL, error, params});
    }
};
