import {LoggedRequest, Result} from "./logger";
import pluralize from 'pluralize';
import {MongoRequest} from "./middlewares";

type MethodLoggingPhase = (req: LoggedRequest & MongoRequest) => void;

interface MethodLogging {
    start: MethodLoggingPhase,
    finish: MethodLoggingPhase,
    error: (req: LoggedRequest & MongoRequest, error: Error) => void,
}

type CountErrorMethodLogging = (req: LoggedRequest & MongoRequest, error: Error) => void;
type AllMethodLogging = MethodLogging & {countError: CountErrorMethodLogging};
type RemoveMethodLogging = MethodLogging & {finishWithoutChanges: MethodLoggingPhase};

export interface ApiLogging {
    create: MethodLogging,
    createMany: MethodLogging,
    all: AllMethodLogging,
    allOfUser: AllMethodLogging,
    find: MethodLogging,
    update: MethodLogging,
    remove: RemoveMethodLogging,
}

export const emptyApiLogging = (): ApiLogging => {
    return {
        create: {
            start: () => {},
            error: () => {},
            finish: () => {}
        },
        createMany: {
            start: () => {},
            error: () => {},
            finish: () => {}
        },
        all: {
            start: () => {},
            error: () => {},
            countError: () => {},
            finish: () => {}
        },
        allOfUser: {
            start: () => {},
            error: () => {},
            countError: () => {},
            finish: () => {}
        },
        find: {
            start: () => {},
            error: () => {},
            finish: () => {}
        },
        update: {
            start: () => {},
            error: () => {},
            finish: () => {}
        },
        remove: {
            start: () => {},
            error: () => {},
            finish: () => {},
            finishWithoutChanges: () => {},
        },
    }
}

export const apiLogging = (logger, entityName): ApiLogging => {
    const entityNames = pluralize(entityName);

    const create: MethodLogging = {
        start: (req) => {
            logger.info(`Creating ${entityName}`, {
                req,
                result: Result.IN_PROGRESS,
            });
        },
        error: (req, error) => {
            logger.error(`Error found on creating ${entityName}`, {
                req,
                error,
            });
        },
        finish: (req) => {
            logger.info(`${entityName} created successfully`, {
                req,
                result: Result.SUCCESS,
            });
        }
    }

    const createMany: MethodLogging = {
        start: (req) => {
            logger.info(`Creating ${entityNames}`, {
                req,
                result: Result.IN_PROGRESS,
            });
        },
        error: (req, error) => {
            logger.error(`Error found on creating ${entityNames}`, {
                req,
                error,
            });
        },
        finish: (req) => {
            logger.info(`${entityNames} created successfully`, {
                req,
                result: Result.SUCCESS,
            });
        }
    }

    const all: AllMethodLogging = {
        start: (req) => {
            logger.info(`Looking for ${entityNames}`, {
                req,
                result: Result.IN_PROGRESS,
                params: {
                    mongoParams: req.mongoParams
                }
            });
        },
        error: (req, error) => {
            logger.error(`Error found on looking for ${entityNames}`, {
                req,
                error,
            });
        },
        countError: (req, error) => {
            logger.error(`Error found on counting ${entityNames}`, {
                req,
                error,
            });
        },
        finish: (req) => {
            logger.info(`${entityNames} found successfully`, {
                req,
                result: Result.SUCCESS,
                params: {
                    mongoParams: req.mongoParams
                }
            });
        }
    }

    const allOfUser: AllMethodLogging = {
        start: (req) => {
            logger.info(`Looking for user ${entityNames}`, {
                req,
                result: Result.IN_PROGRESS,
                params: {
                    mongoParams: req.mongoParams
                }
            });
        },
        error: (req, error) => {
            logger.error(`Error found on looking for user ${entityNames}`, {
                req,
                error,
            });
        },
        countError: (req, error) => {
            logger.error(`Error found on counting user ${entityNames}`, {
                req,
                error,
            });
        },
        finish: (req) => {
            logger.info(`User ${entityNames} found successfully`, {
                req,
                result: Result.SUCCESS,
                params: {
                    mongoParams: req.mongoParams
                }
            });
        }
    }

    const find: MethodLogging = {
        start: (req) => {
            logger.info(`Looking for ${entityName}`, {
                req,
                result: Result.IN_PROGRESS,
                params: {
                    ...req.params,
                    mongoParams: req.mongoParams
                }
            });
        },
        error: (req, error) => {
            logger.error(`Error found on looking for ${entityName}`, {
                req,
                error,
                params: {
                    ...req.params,
                    mongoParams: req.mongoParams
                }
            });
        },
        finish: (req) => {
            logger.info(`${entityName} found`, {
                req,
                result: Result.SUCCESS,
                params: {
                    ...req.params,
                    mongoParams: req.mongoParams
                }
            });
        }
    }

    const update: MethodLogging = {
        start: (req) => {
            logger.info(`Updating ${entityName}`, {
                req,
                result: Result.IN_PROGRESS,
                params: {
                    ...req.params,
                }
            });
        },
        error: (req, error) => {
            logger.error(`Error found on updating for ${entityName}`, {
                req,
                error,
                params: {
                    ...req.params,
                }
            });
        },
        finish: (req) => {
            logger.info(`${entityName} updated`, {
                req,
                result: Result.SUCCESS,
                params: {
                    ...req.params,
                }
            });
        }
    }

    const remove: RemoveMethodLogging = {
        start: (req) => {
            logger.info(`Removing ${entityName}`, {
                req,
                result: Result.IN_PROGRESS,
                params: {
                    ...req.params,
                }
            });
        },
        error: (req, error) => {
            logger.error(`Error found on removing for ${entityName}`, {
                req,
                error,
                params: {
                    ...req.params,
                }
            });
        },
        finish: (req) => {
            logger.info(`${entityName} removed`, {
                req,
                result: Result.SUCCESS,
                params: {
                    ...req.params,
                }
            });
        },
        finishWithoutChanges: (req) => {
            logger.info(`${entityName} not found`, {
                req,
                result: Result.FAIL,
                params: {
                    ...req.params,
                }
            });
        }
    }

    return {
        create,
        createMany,
        all,
        allOfUser,
        find,
        update,
        remove,
    }
}
