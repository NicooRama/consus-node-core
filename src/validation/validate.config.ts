export interface JoiValidateConfig{
    abortEarly: boolean;
    allowUnknown: boolean;
    errors: { label: boolean }
}

export const validateConfig: JoiValidateConfig = {
    abortEarly: false,
    allowUnknown: false,
    errors: { label: false },
};
