import Joi from "joi";

export const timemstampSchema = {
    createdAt: Joi.date(),
    updatedAt: Joi.date(),
};

export const metadataSchema = {
    _id: Joi.string(),
    __v: Joi.number(),
}