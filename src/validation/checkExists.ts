import {NextFunction, Request, Response} from "express";
import {Invalid} from "../errors/Invalid";
import {bodyError} from "../errors/error.factory";
import {UserRequest} from "../user";
import {Model} from "mongoose";

export const nameRegex = (value) => {
    return new RegExp(`^${value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')}$`, 'i');
}

export const checkExistsInSystem = (model, property) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        const value = req.body[property];
        const query: any = {
            [property]: {$regex: nameRegex(value)}
        };
        if(req.params && req.params.id) {
            query._id = {$ne: req.params.id}
        };
        let exists;
        try {
            exists = await model.exists(query);
        } catch (e) {
            return next(e);
        }
        if(!exists) {
            return next();
        }
        next(bodyError(new Invalid(property, 'is already in use')))
    }
}

export type ExistsOptions = {
    modelProperty: string,
    entityProperty: string,
    transformer?: (value) => any,
}

export const exists = async (
        model: Model<any>,
        entity: any,
        options: ExistsOptions
) => {
    let {modelProperty, entityProperty, transformer} = options;
    if(!transformer) {
        transformer = (value) => value;
    }
    const value = transformer(entity)[entityProperty];
    const query: any = {
        [modelProperty]: value
    };
    return !!(await model.exists(query));
}

export const validateExists = (
    {
        model,
        options: {modelProperty, entityProperty, transformer}
    }: {model: Model<any>, options: ExistsOptions}
) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        let entityExists;
        try {
            entityExists = await exists(model, req.body, {modelProperty, entityProperty, transformer});
        } catch (e) {
            return next(e);
        }
        if(entityExists) {
            return next();
        }
        next(bodyError(new Invalid(entityProperty, 'is not exists')))
    }
}

export const validateManyExists = (
    {
        model,
        options: {modelProperty, entityProperty, transformer}
    }: {model: Model<any>, options: ExistsOptions}
) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        let entitiesExists: boolean[];
        let entities = req.body;

        try {
            entitiesExists = await Promise.all<boolean[]>(
                entities.map(entity => exists(model, entity, {modelProperty, entityProperty, transformer}))
            )
        } catch (e) {
            return next(e);
        }
        if(entitiesExists.every(exists => exists)) {
            return next();
        }
        const notExistsIndex = entitiesExists.map((exists, i) => !exists ? i : undefined)
            .filter(i => i !== undefined);
        const notExistsEntities = notExistsIndex.map(i => entities[i]);
        const logMessage = `entities with [${notExistsEntities.map(e => e[entityProperty]).join(', ')}] not exists`;
        next(bodyError(new Invalid(entityProperty, logMessage)))
    }
}

export const existsInUserOrSystem = async (
    {
        model,
        entity,
        property,
        userId,
        params = null,
        userIdKey = 'userId'
    }
) => {
    const value = entity[property];
    const query: any = {
        [property]: {$regex: nameRegex(value)}
    };
    if (userId) {
        query[userIdKey] = userId;
    }
    if (params && params.id) {
        query._id = {$ne: params.id}
    };
    return await model.exists(query);
}

export const checkExistsInUserOrSystem = (model, property, userIdKey = 'userId') => {
    return async (req: UserRequest, res: Response, next: NextFunction) => {
        let exists;
        try {
            exists = await existsInUserOrSystem(
                {
                    model,
                    entity: req.body,
                    property,
                    userId: req.user?._id,
                    params: req.params as any,
                    userIdKey,
                }
            )
        } catch (e) {
            return next(e);
        }
        if(!exists) {
            return next();
        }
        next(bodyError(new Invalid(property, 'is already in use')))
    }
}
