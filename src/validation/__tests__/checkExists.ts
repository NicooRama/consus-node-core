import {
    checkExistsInSystem,
    checkExistsInUserOrSystem,
    exists,
    nameRegex,
    validateExists,
    validateManyExists
} from "../checkExists";
import {bodyError, Invalid} from "../../errors";
import {MongoMemoryServer} from "mongodb-memory-server";
import mongoose from "mongoose";

describe('checkExists', () => {
    describe('nameRegex', () => {
        const match = (word1: string, word2: string, expected = true) => {
            expect(nameRegex(word1).test(word2)).toBe(expected);
        }

        it('matchs same word', () => {
            match('foo', 'foo');
        });

        it('matchs same word with parentheses', () => {
            match('(foo)', '(foo)');
        });

        it('matchs insensitive case', () => {
            match('foo', 'FOO');
        });

        it('dosen\'t match different words', () => {
            match('foo', 'bar', false)
        });
    });

    describe('validateExists', () => {
        let mongoServer: MongoMemoryServer;
        let model;

        beforeAll(async () => {
            mongoServer = await MongoMemoryServer.create();
            const mongoUri = mongoServer.getUri();

            await mongoose.connect(mongoUri, {});

            const modelSchema = new mongoose.Schema({
                name: String,
                userId: String,
            });

            model = mongoose.model('TestModel', modelSchema);
        });

        afterAll(async () => {
            await mongoose.disconnect();
            await mongoServer.stop();
        });

        beforeEach(async () => {
            await mongoose.model('TestModel').deleteMany({});
        })

        describe('exists', () => {
            it('should return exists true because model exists', async () => {
                const _id = new mongoose.Types.ObjectId();
                await new model({_id, name: 'Foo', userId: '1'}).save();
                const entity = {
                    entityId: _id.toString(),
                }
                const transformer = (entity) => (
                    {
                        ...entity,
                        entityId: new mongoose.Types.ObjectId(entity.entityId)
                    }
                );
                const result = await exists(model, entity, {
                    modelProperty: '_id',
                    entityProperty: 'entityId',
                    transformer,
                });
                expect(result).toBe(true);
            });

            it('should use default transformer because transformer is not specified', async () => {
                const _id = new mongoose.Types.ObjectId();
                await new model({_id, name: 'Foo', userId: '1'}).save();
                const entity = {
                    entityId: _id,
                }
                const result = await exists(model, entity, {
                    modelProperty: '_id',
                    entityProperty: 'entityId',
                });
                expect(result).toBe(true);
            });


        });

        describe('validateExists', () => {
            it('should call next without error because model exists', async () => {
                const _id = new mongoose.Types.ObjectId();
                await new model({_id, name: 'Foo', userId: '1'}).save();
                const entity = {
                    entityId: _id.toString(),
                }
                const transformer = (entity) => (
                    {
                        ...entity,
                        entityId: new mongoose.Types.ObjectId(entity.entityId)
                    }
                );
                const req: any = {
                    body: entity,
                };
                const res: any = {};
                const next = jest.fn();
                const callback = validateExists({
                    model,
                    options: {
                        modelProperty: '_id',
                        entityProperty: 'entityId',
                        transformer,
                    }
                });
                await callback(req, res, next);
                expect(next).toBeCalledWith();
            });

            it('should call next with error because model not exists', async () => {
                const _id = new mongoose.Types.ObjectId();
                await new model({_id: new mongoose.Types.ObjectId(), name: 'Foo', userId: '1'}).save();
                const entity = {
                    entityId: _id.toString(),
                }
                const transformer = (entity) => (
                    {
                        ...entity,
                        entityId: new mongoose.Types.ObjectId(entity.entityId)
                    }
                );
                const req: any = {
                    body: entity,
                };
                const res: any = {};
                const next = jest.fn();
                const callback = validateExists({
                    model,
                    options: {
                        modelProperty: '_id',
                        entityProperty: 'entityId',
                        transformer,
                    }
                });
                await callback(req, res, next);
                expect(next).toBeCalledWith(bodyError(new Invalid('entityId', 'is not exists')));
            });
        });

        describe('validateManyExists', () => {
            it('should call next without error because all entities exists', async () => {
                const _id = new mongoose.Types.ObjectId();
                const _id2 = new mongoose.Types.ObjectId();
                await new model({_id, name: 'Foo', userId: '1'}).save();
                await new model({_id: _id2, name: 'Foo2', userId: '1'}).save();
                const transformer = (entity) => (
                    {
                        ...entity,
                        entityId: new mongoose.Types.ObjectId(entity.entityId)
                    }
                );
                const req: any = {
                    body: [
                        {
                            entityId: _id.toString(),
                        },
                        {
                            entityId: _id2.toString(),
                        },
                    ],
                };
                const res: any = {};
                const next = jest.fn();
                const callback = validateManyExists({
                    model,
                    options: {
                        modelProperty: '_id',
                        entityProperty: 'entityId',
                        transformer,
                    }
                });
                await callback(req, res, next);
                expect(next).toBeCalledWith();
            });
            it('should call next with error because one of entities not exists', async () => {
                const _id = new mongoose.Types.ObjectId();
                const _id2 = new mongoose.Types.ObjectId();
                const unexistsId = new mongoose.Types.ObjectId();
                await new model({_id, name: 'Foo', userId: '1'}).save();
                await new model({_id: _id2, name: 'Foo2', userId: '1'}).save();
                const transformer = (entity) => (
                    {
                        ...entity,
                        entityId: new mongoose.Types.ObjectId(entity.entityId)
                    }
                );
                const req: any = {
                    body: [
                        {
                            entityId: _id.toString(),
                        },
                        {
                            entityId: unexistsId.toString(),
                        },
                        {
                            entityId: _id2.toString(),
                        },
                    ],
                };
                const res: any = {};
                const next = jest.fn();
                const callback = validateManyExists({
                    model,
                    options: {
                        modelProperty: '_id',
                        entityProperty: 'entityId',
                        transformer,
                    }
                });
                await callback(req, res, next);
                expect(next).toBeCalledWith(bodyError(new Invalid('entityId', `entities with [${unexistsId.toString()}] not exists`)));
            });
        });
    });


    describe('checkExistsInUserOrSystem', () => {
        const next = jest.fn();
        const model = {
            exists: jest.fn(),
        }

        const req = (name: string, userId?: string, entityId?: string): any => {
            const result: any = {
                body: {
                    name
                }
            };
            if (userId) {
                result.user = {
                    _id: userId
                }
            }
            if (entityId) {
                result.params = {
                    id: entityId
                }
            }
            return result;
        };

        const res: any = {};

        beforeEach(() => {
            next.mockClear();
            model.exists.mockClear();
        })

        it('build property query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const callback = checkExistsInUserOrSystem(model, 'name', 'userId');
            await callback(req('foo'), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                }
            });
            expect(next).toBeCalledWith();
        });

        it('build property and user query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const userId = "1";
            const callback = checkExistsInUserOrSystem(model, 'name', 'userId');
            await callback(req('foo', userId), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                },
                userId,
            });
            expect(next).toBeCalledWith();
        });

        it('build property and params query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const entityId = "1";
            const callback = checkExistsInUserOrSystem(model, 'name', 'userId');
            await callback(req('foo', null, entityId), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                },
                _id: {$ne: entityId}
            });
            expect(next).toBeCalledWith();
        });

        it('build property, user and params query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const userId = "1";
            const entityId = "2";
            const callback = checkExistsInUserOrSystem(model, 'name', 'userId');
            await callback(req('foo', userId, entityId), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                },
                userId,
                _id: {$ne: entityId}
            });
            expect(next).toBeCalledWith();
        });

        it('throws error because entity exists', async () => {
            model.exists.mockResolvedValueOnce(true);
            const userId = "1";
            const entityId = "2";
            const callback = checkExistsInUserOrSystem(model, 'name', 'userId');
            await callback(req('foo', userId, entityId), res, next);
            expect(next).toBeCalledWith(bodyError(new Invalid('name', 'is already in use')));
        })
    });

    describe('checkExistsInSystem', () => {
        const next = jest.fn();
        const model = {
            exists: jest.fn(),
        }

        const req = (name: string, entityId?: string): any => {
            const result: any = {
                body: {
                    name
                }
            };
            if (entityId) {
                result.params = {
                    id: entityId
                }
            }
            return result;
        };

        const res: any = {};

        beforeEach(() => {
            next.mockClear();
            model.exists.mockClear();
        })

        it('build property query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const callback = checkExistsInSystem(model, 'name');
            await callback(req('foo'), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                }
            });
            expect(next).toBeCalledWith();
        });

        it('build property and params query successfully', async () => {
            model.exists.mockResolvedValueOnce(false);
            const entityId = "1";
            const callback = checkExistsInUserOrSystem(model, 'name');
            await callback(req('foo', entityId), res, next);
            expect(model.exists).toBeCalledWith({
                name: {
                    $regex: /^foo$/i
                },
                _id: {$ne: entityId}
            });
            expect(next).toBeCalledWith();
        });

        it('throws error because entity exists', async () => {
            model.exists.mockResolvedValueOnce(true);
            const entityId = "2";
            const callback = checkExistsInUserOrSystem(model, 'name');
            await callback(req('foo', entityId), res, next);
            expect(next).toBeCalledWith(bodyError(new Invalid('name', 'is already in use')));
        })
    });
})
