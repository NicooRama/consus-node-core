import {NextFunction, Request, Response} from "express";
import {JoiValidateConfig, validateConfig} from "./validate.config";
import {joiBodyError} from "../errors/error.factory";

/**
 * Valida que el body de una request cumpla con los criterios de un esquema de joi
 * @param schema
 * @param config
 * @param {"required" | "optional"} presence
 * @returns {(req: e.Request, res: e.Response, next: e.NextFunction) => void}
 */
export const validateBody = (
    /* eslint-disable @typescript-eslint/no-explicit-any */
    schema: any,
    presence: 'required' | 'optional' = 'optional',
    config: JoiValidateConfig = validateConfig,
) => {
    return (req: Request, res: Response, next: NextFunction): void => {
        const { error } = schema.validate(req.body, { ...config, ...{ presence } });
        if (error) {
            return next(joiBodyError(error));
        }
        next();
    };
};
