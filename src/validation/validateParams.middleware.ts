import {NextFunction, Request, Response} from "express";
import {JoiValidateConfig, validateConfig} from "./validate.config";
import {joiParamsError} from "../errors/error.factory";

/**
 * Valida que los parametros de la url de una request cumpla con los criterios de un esquema de joi
 * @param schema
 * @param config
 * @returns {(req: e.Request, res: e.Response, next: e.NextFunction) => void}
 */

export const validateParams = (
    schema: any /* eslint-disable @typescript-eslint/no-explicit-any */,
    config: JoiValidateConfig = validateConfig,
) => {
    return (req: Request, res: Response, next: NextFunction): void => {
        const { error } = schema.validate(req.params, config);
        if (error) {
            return next(joiParamsError(error));
        }
        next();
    };
};
