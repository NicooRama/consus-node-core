import {Request} from 'express';

export interface JwtPayload {
    _id: string,
    username: string,
}

export interface UserRequest extends Request {
    user: JwtPayload
}
