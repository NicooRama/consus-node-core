import {Types} from "mongoose";
import {addUserToBody} from "../user.middlewares";

describe('user.middlewares', () => {
    describe('addUserToBody', () => {
        it('should add user to object body', async () => {
            const userId = new Types.ObjectId().toString();
            const req: any = {
                body: {
                    entityId: 1,
                    name: 'Foo',
                },
                user: {
                    _id: userId,
                }
            }
            const res: any = {};
            const next = jest.fn();
            await addUserToBody()(req, res, next);
            expect(req.body.userId).toBe(userId);
            expect(next).toBeCalledWith();
        });
        it('should add user to array body in all elements', async () => {
            const userId = new Types.ObjectId().toString();
            const req: any = {
                body: [
                    {
                        entityId: 1,
                        name: 'Foo',
                    },
                    {
                        entityId: 2,
                        name: 'Bar',
                    },
                ],
                user: {
                    _id: userId,
                }
            }
            const res: any = {};
            const next = jest.fn();
            await addUserToBody()(req, res, next);
            expect(req.body[0].userId).toBe(userId);
            expect(req.body[1].userId).toBe(userId);
            expect(next).toBeCalledWith();
        });
    });
});

