import {Response, NextFunction} from "express";
import {UserRequest} from "./user.interface";
import {Types} from 'mongoose';
import {constants} from "http2";
import {MongoRequest} from "../middlewares";
import {ApiError} from "../errors";
import {expressjwt, GetVerificationKey, Request as JWTRequest} from "express-jwt";
import jwksRsa from "jwks-rsa";

export const addUserIdToQuery = ({
                                     key = 'userId',
                                     exclusive = false,
                                     string = true, //if is string or ObjectId type
                                 } = {}) => async (req: UserRequest & MongoRequest, res: Response, next: NextFunction): Promise<void> => {
    const userId = string ? req.user?._id : new Types.ObjectId(req.user?._id);
    if (exclusive) {
        req.mongoParams.query[key] = userId;
    } else {
        req.mongoParams.query[key] = {$in: [userId, undefined, null]}
    }
    next();
};

export const addIsSystemElementToQuery = (key = 'userId') => async (req: UserRequest & MongoRequest, res: Response, next: NextFunction): Promise<void> => {
    req.mongoParams.query[key] = null;
    next();
};

export const addUserToBody = ({
                                  key = 'userId',
                                  string = true
                              } = {}) => async (req: UserRequest, res: Response, next: NextFunction): Promise<void> => {
    const userId = string ? req.user._id : new Types.ObjectId(req.user._id);
    if (Array.isArray(req.body)) {
        req.body.forEach((item) => {
            item[key] = userId;
        });
    } else {
        req.body[key] = userId;
    }
    next();
}

export const validateOwnership = (model, {
    paramKey = 'id',
    userKey = 'userId',
    string = true,
} = {}) => async (req: UserRequest, res: Response, next: NextFunction): Promise<void> => {
    const userId = string ? req.user._id : new Types.ObjectId(req.user._id);
    const _id = new Types.ObjectId(req.params[paramKey]);
    let result;
    try {
        result = await model.exists({_id, [userKey]: userId});
    } catch (e) {
        return next(e);
    }
    if (result) {
        return next();
    }
    return next(new ApiError(constants.HTTP_STATUS_UNAUTHORIZED, 'Unauthorized', 'You don\'t have the ownership of the element'));
};

export const validateOwnershipOrSystem = (model, {
    paramKey = 'id',
    userKey = 'userId',
    string = true,
} = {}) => async (req: UserRequest, res: Response, next: NextFunction): Promise<void> => {
    const userId = string ? req.user?._id : new Types.ObjectId(req.user?._id);
    const _id = new Types.ObjectId(req.params[paramKey]);
    let result;
    try {
        result = await model.exists({_id, [userKey]: {$in: [userId, undefined, null]}});
    } catch (e) {
        return next(e);
    }
    if (result) {
        return next();
    }
    return next(new ApiError(constants.HTTP_STATUS_UNAUTHORIZED, 'Unauthorized', 'You don\'t have the ownership of the element'));
};

export const checkAuth0Jwt = ({
                                  domain,
                                  audience
                              }) => expressjwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksUri: `https://${domain}/.well-known/jwks.json`,
    }) as GetVerificationKey,
    audience: audience,
    issuer: `https://${domain}/`,
    algorithms: ["RS256"],
});

export const auth0ToUserRequest = (customKey: string) => (req: JWTRequest & UserRequest, res: Response, next: any) => {
    if (req.auth.sub === undefined) {
        next();
        return;
    }
    req.user = {
        _id: req.auth.sub,
        username: req.auth[customKey].email,
    };
    next();
}

export const auth0ErrorMiddleware = (err, req, res, next)  => {
    if(err.name === 'UnauthorizedError'){
        return next(new ApiError(constants.HTTP_STATUS_UNAUTHORIZED, 'Unauthorized', err.code));
    }
    next(err);
}
