import * as jwt from 'jsonwebtoken';
import {JwtPayload, UserRequest} from "./user.interface";
import {Response, NextFunction} from "express";
import {constants} from "http2";
import {ApiError} from "../errors";

const verify = (
    token: string,
    secret: string,
): JwtPayload => {
    try {
        return jwt.verify(token, secret) as JwtPayload;
    } catch (error) {
        throw error;
    }
};

export const validateToken = (jwtSecret: string) =>  async (req: UserRequest, res: Response, next: NextFunction): Promise<void> => {
    const authorization: string = req.header('Authorization');
    if (!authorization) {
        return next(new ApiError(constants.HTTP_STATUS_UNAUTHORIZED, 'Unauthorized', 'Token is not present'));
    };
    const token: string = authorization.substr(authorization.indexOf(' ') + 1);
    let payload: JwtPayload;
    try{
        payload = verify(token, jwtSecret);
    }catch(e){
        return next(new ApiError(constants.HTTP_STATUS_UNAUTHORIZED, 'Unauthorized', 'Token is not present'));
    }
    req.user = payload;
    next();
}
