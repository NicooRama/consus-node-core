import {
    containsFilter,
    includesFilter,
    notNullFilter,
    nullFilter,
    parseFilters,
    someFilter,
    withIdFilter
} from "../queryParams.middleware";
import {Types} from "mongoose";

describe('queryParams.middleware', () => {
    describe('filters', () => {
        describe.skip('someFilter', () => {
            describe('condition', () => {
                it('should return true if value starts with some:', () => {
                    expect(someFilter.condition('some:')).toBeTruthy();
                });
                it('should return false if value does not start with some:', () => {
                    expect(someFilter.condition('otherwise')).toBeFalsy();
                });
            });
            describe('modifier', () => {
                it('should return query with some filter', () => {
                    const query = {
                        name: 'some:pepe'
                    };
                    const expected = {
                        name: {$some: 'pepe'}
                    };
                    someFilter.modifier({query, key: 'name', value: query.name});
                    expect(query).toEqual(expected);
                });
            });
        })

        describe('withIdFilter', () => {
            describe('condition', () => {
                it('should return true if value starts with withId:', () => {
                    expect(withIdFilter.condition('withId:')).toBeTruthy();
                });
                it('should return false if value does not start with withId:', () => {
                    expect(withIdFilter.condition('otherwise')).toBeFalsy();
                });
            });

            describe('modifier', () => {
                it('should return query with withId filter', () => {
                    const query = {
                        name: 'withId:64bfccc181a45f80d579faae|64bfcccd3624493a31346321|64bfccd26040a6ae1a831b33'
                    };
                    const expected = {
                        name: {$in: [new Types.ObjectId("64bfccc181a45f80d579faae"), new Types.ObjectId("64bfcccd3624493a31346321"), new Types.ObjectId("64bfccd26040a6ae1a831b33")]}
                    };
                    withIdFilter.modifier({query, key: 'name', value: query.name});
                    expect(query).toEqual(expected);
                });
            });
        });

        describe('containsFilter', () => {
            describe('condition', () => {
                it('should return true if value starts with contains:', () => {
                    expect(containsFilter.condition('contains:')).toBeTruthy();
                });
                it('should return false if value does not start with contains:', () => {
                    expect(containsFilter.condition('otherwise')).toBeFalsy();
                });
            });
            describe('modifier', () => {
                it('should return query with contains filter', () => {
                    const query = {
                        name: 'contains:pepe'
                    };
                    const expected = {
                        name: (new RegExp('pepe', 'i') as any)
                    };
                    containsFilter.modifier({query, key: 'name', value: query.name});
                    expect(query).toEqual(expected);
                });
            });
        });

        describe('includesFilter', () => {
            describe('condition', () => {
                it('should return true if value starts with includes:', () => {
                    expect(includesFilter.condition('foo|bar')).toBeTruthy();
                });
                it('should return false if value does not start with includes:', () => {
                    expect(includesFilter.condition('otherwise')).toBeFalsy();
                });
            });

            describe('modifier', () => {
                it('should return query with includes filter', () => {
                    const query = {
                        name: 'foo|bar'
                    };
                    const expected = {
                        name: {$in: ['foo', 'bar']}
                    };
                    includesFilter.modifier({query, key: 'name', value: query.name});
                    expect(query).toEqual(expected);
                });
            });
        });

        describe('nullFilter', () => {
            describe('condition', () => {
                it('should return true if value is null', () => {
                    expect(nullFilter.condition('null')).toBeTruthy();
                });
                it('should return false if value is not null', () => {
                    expect(nullFilter.condition('otherwise')).toBeFalsy();
                });
            });
            describe('modifier', () => {
                it('should return query with null filter', () => {
                    const query = {
                        name: 'null'
                    };
                    const expected = {
                        name: null
                    };
                    nullFilter.modifier({query, key: 'name', value: query.name});
                    expect(query).toEqual(expected);
                });
            });
        })

        describe('notNullFilter', () => {
            describe('condition', () => {
                it('should return true if value is not null', () => {
                    expect(notNullFilter.condition('!null')).toBeTruthy();
                });
                it('should return false if value is null', () => {
                    expect(notNullFilter.condition('null')).toBeFalsy();
                });
                it('should return false if value is otherwise', () => {
                    expect(notNullFilter.condition('otherwise')).toBeFalsy();
                });
            })
            describe('modifier', () => {
                it('should return query with not null filter', () => {
                    const query = {
                        name: '!null'
                    };
                    const expected = {
                        name: {$ne: null}
                    };
                    notNullFilter.modifier({query, key: 'name'});
                    expect(query).toEqual(expected);
                });
            });
        });

        describe('parseFilters', () => {
            it('parse all filters successfully', () => {
                const query = 'name=contains:pepe,' +
                    'lastname=null,' +
                    'age=!null,' +
                    'productId=withId:64bfccc181a45f80d579faae|64bfcccd3624493a31346321|64bfccd26040a6ae1a831b33,' +
                    'address=foo|bar';
                const expected = {
                    name: (new RegExp('pepe', 'i') as any),
                    lastname: null,
                    age: {$ne: null},
                    productId: {$in: [new Types.ObjectId("64bfccc181a45f80d579faae"), new Types.ObjectId("64bfcccd3624493a31346321"), new Types.ObjectId("64bfccd26040a6ae1a831b33")]},
                    address: {$in: ['foo', 'bar']}
                }
                const result = parseFilters(query);
                expect(result).toEqual(expected);
            });

            it('foo', () => {
                const query = `variant=normal,observations=contains:Btate`;

                const expected = {
                    variant: 'normal',
                    observations: (new RegExp('Btate', 'i') as any),
                }
                const result = parseFilters(query);
                expect(result).toEqual(expected);
            })
        })
    })
});
