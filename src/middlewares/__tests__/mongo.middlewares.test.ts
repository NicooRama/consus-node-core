import {addForeignLookupWithStringField, addLookup, addPopulate, addProjection} from "../mongo.middlewares";

describe('mongo.middlewares', () => {
    describe('addLookup', () => {
        it('should add lookup agreggations to query params, with $unwind aggregation because singleElement is true', () => {
            const req: any = {
                mongoParams: {}
            };
            const res: any = {};
            const next = jest.fn();

            const lookup = {
                from: "products",
                let: {productId: {$toObjectId: "$productId"}},
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$productId"]
                            }
                        }
                    },
                ],
                as: "product"
            }

            addLookup(lookup)(req, res, next);
            expect(req.mongoParams.lookup).toEqual(
                [
                    {
                        $lookup: lookup
                    },
                    {
                        $unwind: `$product`
                    }
                ]
            )
            expect(next).toBeCalled();
        })

        it('should add lookup agreggations to query params, without $unwind aggregation because singleElement is false', () => {
            const req: any = {
                mongoParams: {}
            };
            const res: any = {};
            const next = jest.fn();

            const lookup = {
                from: "products",
                let: {productId: {$toObjectId: "$productId"}},
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$productId"]
                            }
                        }
                    },
                ],
                as: "product"
            }

            addLookup(lookup, {singleElement: false})(req, res, next);
            expect(req.mongoParams.lookup).toEqual(
                [
                    {
                        $lookup: lookup
                    }
                ]
            )
            expect(next).toBeCalled();
        })
    });

    describe('addForeignLookupWithStringField', () => {
       it('should transform arguments passed to lookup aggregation', () => {
           const req: any = {
               mongoParams: {}
           };
           const res: any = {};
           const next = jest.fn();
           addForeignLookupWithStringField('products', 'productId', 'product')(req, res, next);
           expect(req.mongoParams.lookup).toEqual(
               [
                   {
                       $lookup: {
                           from: "products",
                           let: {productId: {$toObjectId: "$productId"}},
                           pipeline: [
                               {
                                   $match: {
                                       $expr: {
                                           $eq: ["$_id", "$$productId"]
                                       }
                                   }
                               },
                           ],
                           as: "product"
                       }
                   },
                   {
                       $unwind: `$product`
                   }
               ]
           )
           expect(next).toBeCalled();
       });
    });

    describe('addPopulate', () => {
        it('should add populate agreggations to query params', () => {
            const req: any = {
                mongoParams: {}
            };
            const res: any = {};
            const next = jest.fn();

            const populate = {
                path: 'product',
                select: 'name'
            }

            addPopulate(populate)(req, res, next);
            expect(req.mongoParams.populate).toEqual(
                [populate]
            )
            expect(next).toBeCalled();
        })
    });

    describe('addProjection', () => {
       it('should add projection to query params', () => {
              const req: any = {
                mongoParams: {}
              };
              const res: any = {};
              const next = jest.fn();

              const projection = {
                name: 1,
                price: 1
              }

              addProjection(projection)(req, res, next);
              expect(req.mongoParams.projection).toEqual(
                projection
              )
              expect(next).toBeCalled();
       });
       it('should merge projection before adding it to query params', () => {
                const req: any = {
                    mongoParams: {
                        projection: {
                            name: 1
                        }
                    }
                };
                const res: any = {};
                const next = jest.fn();

                const projection = {
                    price: 1
                }

                addProjection(projection)(req, res, next);
                expect(req.mongoParams.projection).toEqual(
                    {
                        name: 1,
                        price: 1
                    }
                )
                expect(next).toBeCalled();
       });
    });
})
