import {Request, Response, NextFunction} from 'express';
import {ApiError} from "../errors";

export const apiKeyValidator = (apiKey: string) =>  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    if(req.headers['x-api-key'] === apiKey) {
        return next();
    }
    return next(new ApiError(403, 'x-api-key not provided or invalid', "Forbidden"));
}
