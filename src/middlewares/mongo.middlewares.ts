import {projectionToObject} from "../utils/mongo.utils";

export const addLookup = (lookup: {
    from?: string;
    let?: any;
    localField?: string;
    foreignField?: string;
    pipeline?: object[];
    as: string;
}, {singleElement} = {singleElement: true}) => {
    return (req, res, next) => {
        const field = lookup.as;
        const aggregation: any[] = [
            {
                $lookup: lookup
            }
        ]
        if (singleElement) {
            aggregation.push({
                $unwind: `$${field}`
            })
        }
        req.mongoParams.lookup = aggregation;
        next();
    };
};

export const addForeignLookupWithStringField = (from: string, field: string, finalFieldName?: string, foreignField = '_id') => {
    return addLookup({
        from: from,
        let: { [field]: { $toObjectId: `$${field}` } },
        pipeline: [
            {
                $match: {
                    $expr: {
                        $eq: [`$${foreignField}`, `$$${field}`]
                    }
                }
            },
        ],
        as: finalFieldName || field
    }, {singleElement: true})
}

export const addPopulate = (...populateSentences: any[]) => {
    return (req, res, next) => {
        if(!req.mongoParams) {
            req.mongoParams = {};
        }
        req.mongoParams.populate = [...populateSentences];
        next();
    };
}

export const addProjection = (projection: Record<string, number>) => {
    return (req, res, next) => {
        const previousProjection = projectionToObject(req.mongoParams.projection);
        req.mongoParams.projection = {...previousProjection, ...projection};
        next();
    };
}
