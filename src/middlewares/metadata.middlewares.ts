export const addUpdatedAt = (req, res, next) => {
    req.body.updatedAt = new Date();
    next();
};
