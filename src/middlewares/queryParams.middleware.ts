import qs, {ParsedQs} from 'qs';
import {Request, Response, NextFunction} from 'express';
import {createObjectFromString} from "../utils";
import {Types} from "mongoose";

/* eslint-disable @typescript-eslint/no-explicit-any */
export interface MongoParams {
    query: Record<string, any> | null;
    projection: Record<string, any> | null;
    sort: Record<string, any> | null;
    specialSort: string | null;
    skip: number;
    limit: number;
    lookup?: any[];
    populate?: any[];
}

export interface MongoRequest extends Request {
    mongoParams: MongoParams;
}

interface QueryModifier {
    value?: any;
    query: object;
    key: string;
}

export const Filter = (condition: (value: any) => boolean, modifier: (params: QueryModifier) => any) => (
    {
        condition,
        modifier
    }
)

export const someFilter = Filter(
    (value: any) => value.startsWith('some:'),
    ({value, query, key}) => {
        // separo las keys
        const keys = key.split('.');
        const firstKey = keys.shift();
        const reference = {};
        //vuelvo a unir las keys menos la primera y lo paso a la funcion
        const values = value.replace('some:', '').split('|');
        if (values[0]) {
            createObjectFromString(reference, keys.join('.'), {$in: values});
            query[firstKey] = {$elemMatch: reference};
        }
        delete query[key];
    }
);


export const withIdFilter = Filter(
    (value: any) => value.startsWith('withId:'),
    ({value, query, key}) => {
        const values = value.replace('withId:', '')
            .split('|')
            .map(id => new Types.ObjectId(id));
        query[key] = {$in: values} as any;
    }
);

export const containsFilter = Filter(
    (value: any) => value.startsWith('contains:'),
    ({value, query, key}) => query[key] = (new RegExp(value.replace('contains:', ''), 'i') as any)
);
export const includesFilter = Filter(
    (value: any) => value.includes('|'),
    ({query, key}) => query[key] = {$in: (query[key] as string).split('|')} as any
);

export const nullFilter = Filter(
    (value: any) => value === 'null',
    ({query, key}) => query[key] = null
);

export const notNullFilter = Filter(
    (value: any) => value === '!null',
    ({query, key}) => query[key] = {$ne: null}
);

const filters = [
    nullFilter,
    notNullFilter,
    containsFilter,
    someFilter,
    withIdFilter,
    includesFilter
]

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const parseFilters = (queryFilters: string | string[] | ParsedQs | ParsedQs[]): any => {
    const query = queryFilters ? qs.parse(queryFilters.toString(), {delimiter: ','}) : null;
    if (!query) return {};

    Object.keys(query).forEach((key: string) => {
        const value = query[key] as string;

        for (let i = 0; i < filters.length; i++) {
            const filter = filters[i];
            if (filter.condition(value)) {
                filter.modifier({value, query, key});
                return;
            }
        }
    });

    return query;
};

export const parseStringToObject = (fields, negativeValue = 0) => {
    const strAggregation = (fields as string).split(',');
    const objectAggregation = {};
    strAggregation.forEach(sp => {
        if (sp.charAt(0) === '-') {
            objectAggregation[sp.substring(1)] = negativeValue
        } else {
            objectAggregation[sp] = 1
        }
    });
    return objectAggregation;
}

/**
 * Transforma los query params en parametros que espera mongo al momento de realizar una query
 *
 * @param {e.Request} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 */
export const parseQueryParams = (req: MongoRequest, res: Response, next: NextFunction): void => {
    const {filters, fields, sort, specialSort, offset, limit} = req.query as any;

    // filters[]=basicData.name=Martin,basicData.surname=Ferrari alvarez,email=martin.ferrari.alvarez@gmail.com
    // fields=basicData.name,basicData.surname
    // sort=name,-age

    req.mongoParams = {
        query: parseFilters(filters),
        projection: fields ? parseStringToObject(fields) : null,
        sort: sort ? parseStringToObject(sort, -1) : null,
        specialSort: specialSort as string || null,
        skip: parseInt(offset as string),
        limit: parseInt(limit as string),
    };
    return next();
};
