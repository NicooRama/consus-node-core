import {projectionToObject} from "../mongo.utils";

describe('mongo.utils', () => {
    describe('projectionToObject', () => {
        it('should convert string projection to object projection', () => {
            const strProjection = "name -password email";
            const objProjection = projectionToObject(strProjection);
            expect(objProjection).toEqual({ name: 1, password: 0, email: 1 });
        });

        it('should return object projection', () => {
            expect(projectionToObject({ name: 1, password: 0, email: 1 })).toEqual({ name: 1, password: 0, email: 1 });
        });

        it('should return empty projection because strProjection is null', () => {
            expect(projectionToObject(null)).toEqual({});
        });
        it('should return empty projection because strProjection is undefined', () => {
            expect(projectionToObject(undefined)).toEqual({});
        });
    })
})
