export function projectionToObject(strProjection: string | object): Record<string, number> {
    if(strProjection === null || strProjection === undefined) return {};
    if(typeof strProjection === 'object') return strProjection as Record<string, number>;
    let projection: Record<string, number> = {};
    let fields = strProjection.split(' ');

    fields.forEach(field => {
        if (field.startsWith('-')) {
            // if field starts with "-", exclude this field
            projection[field.substring(1)] = 0;
        } else {
            // otherwise, include this field
            projection[field] = 1;
        }
    });

    return projection;
}
