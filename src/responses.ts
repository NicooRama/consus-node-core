export const success = (message: string, data?: any) => (
    {
        message,
        data,
    }
);